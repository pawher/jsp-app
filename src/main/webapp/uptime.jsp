<%@ page import="java.lang.management.RuntimeMXBean" %>
<%@ page import="java.lang.management.ManagementFactory" %>
<%@page language="java" contentType="text/html;charset=utf-8"%>
<jsp:include page="header.jsp"/>
<h1>uptime</h1>

<%
    RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    long uptime = runtimeMXBean.getUptime()/1000;

    if (uptime<60) {
        out.print(uptime + "sec");
    } else if (uptime>60 && uptime <3600) {
        out.print(uptime/60 + "min" + uptime + "sec");
    } else {
        out.print(uptime/3600 + "h" + uptime/60 + "min" + uptime + "sec");
    }

%>
<jsp:include page="footer.jsp"/>
</html>